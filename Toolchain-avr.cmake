# OS
SET(CMAKE_SYSTEM_NAME Generic)

# Constants
SET(GNU_HOST avr)
SET(PATH_TO_HOST /usr/bin)

# Global Constants
SET(AVRDUDE avrdude)

# Compilers
SET(CMAKE_C_COMPILER ${PATH_TO_HOST}/${GNU_HOST}-gcc)
SET(CMAKE_CXX_COMPILER ${PATH_TO_HOST}/${GNU_HOST}-g++)
