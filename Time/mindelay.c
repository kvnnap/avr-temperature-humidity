#include "mindelay.h"

// Up to 15937.5ns, increments 62.5ns
void waitNanoSeconds(uint16_t ns) {
	// No Prescaling
	TCCR0B = (1 << CS00);
	TCNT0 = 0;
	while(ns > (TCNT0 * T_CPU_NS)){}
	return;
}

// Up to 127.5us -> increments 0.5us
void waitMicroSeconds(uint16_t us) {
	// 1/8th Prescaling
	TCCR0B = (1 << CS01);
	TCNT0 = 0;
	while(us > (TCNT0 * (T_CPU_US * 8))) {}
	return;
}

// Up to 16.32ms and more since it loops -> increments 0.064ms
void waitMilliSeconds(uint16_t ms) {
	// 1/1024 Prescaing
	TCCR0B = (1 << CS02) | (1 << CS00);
	const uint16_t cycles = (uint16_t) (ms / (T_CPU_MS * 1024 * 255));
	{
		uint16_t i;
		for (i = 0; i < cycles; ++i) {
			TCNT0 = 0;
			while(TCNT0 != 255) {}
		}
	}
	{
		float fms = ms - cycles * T_CPU_MS * 1024 * 255;
		fms = fms < 0 ? 0 : fms;
		TCNT0 = 0;
		while(fms > (TCNT0 * (T_CPU_MS * 1024))) {}
	}
	return;
}

void resetMicro() {
	// 1/8th Prescaling
	TCCR0B = (1 << CS01);
	TCNT0 = 0;
}

float micro() {
	uint8_t t = TCNT0;
	TCCR0B = TCNT0 = 0;
	return t * (T_CPU_US * 8);
}

// Uses Timer 1
void resetMicroBroad() {
	// 1/64th Prescaling
	TCCR1B = (1 << CS11) | (1 << CS10);
	TCNT1 = 0;
}

// Up to 262140us -> increments 4us
float microBroad() {
	uint16_t t = TCNT1;
	TCCR1B = TCNT1 = 0;
	return t * (T_CPU_US * 64);
}

// fast pwm
void setup_fpwm_2() {
	DDRB  |= 0x08; //set PB3 as output
	//setup timer using timer control register
	TCCR2A |=  (1 << COM2A1) | (1 << WGM21) | (1 << WGM20); // = 0x83; //non-inverting, FAST PWM MODE
	TCCR2B |=  /*(1 << CS22) | (1 << CS21) | */(1 << CS20); // = 0x01 - no prescaling
	//timer is now running
}

void set_duty_on(uint8_t n) {
	//set Output Compare Register A
	OCR2A = n;
}