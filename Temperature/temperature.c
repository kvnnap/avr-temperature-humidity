#include "temperature.h"
#include "Time/mindelay.h"
#include <avr/io.h>

void setupTemperatureHumidity() {
	waitMilliSeconds(2000);
}

void startup() {
	// Lower the signal for 18 ms
	TEMPERATURE_PORT &= TEMPERATURE_DATA_MASK;

	// Set up direction as output
	TEMPERATURE_DDR |= TEMPERATURE_DATA;

	// Wait for 18ms
	waitMilliSeconds(18);

	// Pull up voltage for 40us
	TEMPERATURE_PORT |= TEMPERATURE_DATA;

	// Wait 40us
	waitMicroSeconds(40);

	// Set up direction as input
	TEMPERATURE_DDR &= TEMPERATURE_DATA_MASK;

} 

void processDHTStartup() {
	
	// Loop until signal becomes zero and wait for 80us
	while (TEMPERATURE_PIN & TEMPERATURE_DATA);

	// wait for 80us
	// waitMicroSeconds(80);

	// Loop until signal becomes one and wait for 80us
	while ((TEMPERATURE_PIN & TEMPERATURE_DATA) == 0);

	// wait for 80us
	// waitMicroSeconds(80);

	// Loop until signal becomes zero to start reading signal
	while (TEMPERATURE_PIN & TEMPERATURE_DATA);
}

uint8_t readByte() {
	uint8_t byte = 0;
	int i;

	for (i = 0; i < 8; ++i) {
		// wait for 50us
	  	//waitMicroSeconds(50);

	  	// Loop until signal becomes one
		while ((TEMPERATURE_PIN & TEMPERATURE_DATA) == 0);

		// Determine whether bit is 1 or 0 depending on length
		resetMicro();
		while (TEMPERATURE_PIN & TEMPERATURE_DATA);

		byte <<= 1;
		if (micro() > 28) {
			byte |= 1;
		}
	}

	return byte;
}

uint8_t readTemperatureAndHumidity(struct TemperatureHumidity* pointerToStruct) {
	if (pointerToStruct == 0) {
		return 0;
	}

	// Let's start reading DATA!!!
	setupTemperatureHumidity();
	startup();
	processDHTStartup();

	pointerToStruct->humidity.integral = readByte();
	pointerToStruct->humidity.decimal = readByte();
	pointerToStruct->temperature.integral = readByte();
	pointerToStruct->temperature.decimal = readByte();

	uint8_t checksum = readByte();

	// Loop until signal becomes one - End of frame
	while ((TEMPERATURE_PIN & TEMPERATURE_DATA) == 0);

	// Compute checksum
	uint8_t computedChecksum = pointerToStruct->humidity.integral + pointerToStruct->humidity.decimal
	 + pointerToStruct->temperature.integral + pointerToStruct->temperature.decimal;

	return computedChecksum == checksum;
}