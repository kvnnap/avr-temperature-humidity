#ifndef TEMPERATURE
#define TEMPERATURE

#include <avr/io.h>
#include <inttypes.h>

#define TEMPERATURE_DDR DDRD
#define TEMPERATURE_PORT PORTD
#define TEMPERATURE_PIN PIND

#define TEMPERATURE_DATA (1 << 7)
#define TEMPERATURE_DATA_MASK ~(TEMPERATURE_DATA)

struct  Number {
	uint8_t integral;
	uint8_t decimal;
};

struct TemperatureHumidity {
	struct Number humidity;
	struct Number temperature;
};

uint8_t readTemperatureAndHumidity(struct TemperatureHumidity* pointerToStruct);

#endif