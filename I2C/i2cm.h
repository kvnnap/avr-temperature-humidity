#ifndef I2C_M
#define I2C_M

#define TW_WRITE 0
#define TW_READ 1

// SCL frequency
#define F_SCL 100000UL
#define PRESCALER 1

// // Read or Write from I2C address. I2C address is 7 bits long
// #define I2C_READ 0x01
// #define I2C_WRITE 0x00

#include <inttypes.h>

// Constructor / Destructor
void i2cm_init();
void i2cm_destroy();

// Master Protocol stuff
uint8_t i2cm_start(uint8_t address);
void i2cm_stop();

// IO
uint8_t i2cm_write(uint8_t datum);
uint8_t i2cm_read(uint8_t * datum, uint8_t ack);

#endif
