#include <avr/io.h>
#include <util/twi.h>

#include "i2cm.h"

void i2cm_init() {
	TWSR = 0; // Set prescaler to 1	(TODO: not hard coded)
	TWBR = (uint8_t)(F_CPU/F_SCL - 16) / (2 * PRESCALER);
	TWCR = (1 << TWEN);
}

void i2cm_destroy() {
	TWCR = 0;
}

void i2cm_stop() {
	TWCR = (1 << TWINT) | (1 << TWSTO) | (1 << TWEN);
	while (TWCR & (1 << TWSTO));
}

uint8_t i2cm_start(uint8_t address) {

	// twsr status
	uint8_t twsr;

	// Transmit START condition
	TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);

	// Wait till TWI has transmitted
	while (!(TWCR & (1 << TWINT)));

	// Check status register for success of START
	twsr = TWSR & 0xF8;
	if ((twsr != TW_START) && (twsr != TW_REP_START))
		return 1;

	// Load TWDR with Slave Address + Read|Write
	TWDR = address;

	// Transmit address
	TWCR = (1 << TWINT) | (1 << TWEN);

	// Wait till TWI has transmitted
	while (!(TWCR & (1 << TWINT)));
	
	// Read Status - Success values for us are 0x18 and 0x40
	twsr = TWSR & 0xF8;
	if ((twsr != TW_MT_SLA_ACK) && (twsr != TW_MR_SLA_ACK))
		return 1;

	return 0;
}

uint8_t i2cm_write(uint8_t datum) {

	// Write Data
	TWDR = datum;

	// // Check if datum was accepted in TWDR
	// if (TWCR && (1 << TWWC))
	// 	return 1;

	// Transmit datum
	TWCR = (1 << TWINT) | (1 << TWEN);

	// Wait till TWI has transmitted
	while (!(TWCR & (1 << TWINT)));

	// Read Status - Success values for us are 0x28
	return (TWSR & 0xF8) != TW_MT_DATA_ACK;
}

// Ack should be sent when more data to be read is expected
uint8_t i2cm_read(uint8_t * datum, uint8_t ack) {
	// Start receiving datum
	TWCR = (1 << TWINT) | (ack ? (1 << TWEA) : 0) | (1 << TWEN);
	
	// Wait till datum received and ack/nack sent back
	while (!(TWCR & (1 << TWINT)));

	// Read received data - DATUM must be a valid pointer
	*datum = TWDR;

	uint8_t twsr = TWSR & 0xF8;
	return  ack ? twsr != TW_MR_DATA_ACK : twsr != TW_MR_DATA_NACK;
}