#include <avr/io.h>
#include <inttypes.h>
#include <stdio.h>
#include "Screen/i2clcd.h"
#include "Temperature/temperature.h"

int __attribute__((noreturn)) main(void)
{

    // Initialise Screen
    lcd_init(true, false);
    lcd_clear_display();

    // Write To Screen
    lcd_write_string("Temp & Humidity");
    char buffer[6];

    while(1) {
        // Get sensor data
        struct TemperatureHumidity tempHumidity;
        uint8_t valid = readTemperatureAndHumidity(&tempHumidity);

        if (valid) {
        // 
            uint8_t sizeOfWrite = snprintf(
                buffer, sizeof(buffer), "%d.%d",
                tempHumidity.humidity.integral, tempHumidity.humidity.decimal);

            // Print out Humidity
            lcd_set_DDRAM_address(0x40);
            lcd_write_string("H=");
            lcd_write_string(buffer);

            sizeOfWrite = snprintf(
                buffer, sizeof(buffer), "%d.%d",
                tempHumidity.temperature.integral, tempHumidity.temperature.decimal);

            // Print out Temperature
            lcd_set_DDRAM_address(0x48);
            lcd_write_string("T=");
            lcd_write_string(buffer);
        } else {
            lcd_set_DDRAM_address(0x40);
            lcd_write_string("Invalid Checksum");
        }

        DDRD |= (1 << 6);
        PORTD ^= (1 << 6);
    }
}
