#include <inttypes.h>
#include <stdio.h>

#include "Time/mindelay.h"
#include "ultrasonic.h"

void us_init() {
	// Setup Port Direction initially
	// Set as output
	US_TRIGGER_DDR |= US_TRIGGER;
	// Set output to Low
	US_TRIGGER_PORT &= MASK_US_TRIGGER;

	// Set as Input
	US_ECHO_DDR &= MASK_US_ECHO;
	// Disable Pullup Resistor - no need to enable
	// since it is actively driven by sensor
	US_ECHO_PORT &= MASK_US_ECHO;
	// Enable Pullup Resistor
	//US_ECHO_PORT |= US_ECHO;
}

// Gets distance from object
// Timer may underestimate distance by 0.68mm, 
// (meaning us_distance <= distance <= us_distance + 0.68)
// but this is ok since sensor has +- 2mm error
float us_distance() {
	// Trigger 10us on Trigger Pin
	US_TRIGGER_PORT |= US_TRIGGER;
	waitMicroSeconds(10);
	US_TRIGGER_PORT &= MASK_US_TRIGGER;

	// Wait for input to change to High
	while((US_ECHO_PIN & US_ECHO) == 0) {}

	// Pin is HIGH, start measuring time
	resetMicroBroad();
	while((US_ECHO_PIN & US_ECHO) == US_ECHO) {}
	float us = microBroad();
	
	// Got the value in us, translate to distance
	const float temperature = 20.f;
	float speedOfSound = 331.4f + .6f * temperature;

	// Calculate distance
	float distance = speedOfSound * (us / 1000000.f);
	// Half that because of roundtrip
	distance *= .5f;

	return distance;
}

