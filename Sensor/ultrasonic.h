#ifndef ULTRASONIC
#define ULTRASONIC

#include <avr/io.h>

#define US_TRIGGER_PIN  PINB
#define US_TRIGGER_DDR  DDRB
#define US_TRIGGER_PORT PORTB

#define US_ECHO_PIN  PINB
#define US_ECHO_DDR  DDRB
#define US_ECHO_PORT PORTB

#define US_TRIGGER 0x01
#define US_ECHO 0x02

#define MASK_US_TRIGGER (~US_TRIGGER)
#define MASK_US_ECHO (~US_ECHO)

void us_init();
float us_distance();

#endif
