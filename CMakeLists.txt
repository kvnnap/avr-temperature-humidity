# CMAKE Version and Project Name
cmake_minimum_required(VERSION 3.9)

set(NAME avr-temperature-humidity)
project(${NAME})

# Names
set(EXECUTABLE_NAME ${NAME}.elf)
set(HEX_NAME ${NAME}.hex)

# MCU
set(AVR_MCU atmega328p)

# CPU Frequency
set(AVR_CPU_FREQUENCY 16000000)

set(CMAKE_BUILD_TYPE MinSizeRel)
#set(CMAKE_VERBOSE_MAKEFILE ON)

# Global include directories
include_directories(.)

# Compile options
set(COMPILER_FLAGS -mmcu=${AVR_MCU})
set(LINKER_FLAGS "-Wl,-u,vfprintf -lprintf_flt -lm")
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${COMPILER_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${COMPILER_FLAGS}")

# Compiler preprocessor definitions
add_definitions(-DF_CPU=${AVR_CPU_FREQUENCY})

# avr-test target
# Find all source files in the below directory automatically
file(GLOB_RECURSE SRC
    I2C/*.c
    Screen/*.c
    Sensor/*.c
    Time/*.c
    Temperature/*.c
)
add_executable(${EXECUTABLE_NAME} main.c ${SRC})
set_target_properties(${EXECUTABLE_NAME} PROPERTIES LINK_FLAGS ${LINKER_FLAGS})

# Commands
add_custom_command(
   OUTPUT ${HEX_NAME}
   COMMAND ${CMAKE_OBJCOPY} -O ihex ${EXECUTABLE_NAME} ${HEX_NAME}
   DEPENDS ${EXECUTABLE_NAME}
)

add_custom_target(hex DEPENDS ${HEX_NAME})
add_custom_target(flashhex 
   COMMAND ${AVRDUDE} -c usbasp -p ${AVR_MCU} -U flash:w:${HEX_NAME}:i
   DEPENDS ${HEX_NAME}
)

add_custom_target(elf DEPENDS ${EXECUTABLE_NAME})
add_custom_target(flashelf
   COMMAND ${AVRDUDE} -c usbasp -p ${AVR_MCU} -U flash:w:${EXECUTABLE_NAME}:e
   DEPENDS ${EXECUTABLE_NAME}
)
